# espressobin-hacks
There are two scripts that can be used to build U-Boot and Linux kernel for the [Espressobin board](https://espressobin.net/).

While there is some documentation on how to do this on the [official wiki](http://wiki.espressobin.net/tiki-index.php?page=Build+From+Source+-+Bootloader), the info is obsolete and even when I tired to follow it, I ended up with a non-working U-Boot. Then there is [Armbian](https://www.armbian.com/) which does support the Espressobin and has quite up-to-date U-Boot and kernel, but their [build scripts](https://github.com/armbian/build/) are convoluted and it's not easily possible to just build a custom U-Boot or kernel.

The scripts here were created by deciphering how Armbian builds U-Boot and kernel.

This is not a one-click solution. I wanted the scripts to be understandable and to make the build steps obvious. I did not want to create another convoluted system replicating what Armbian does.

## Building U-Boot

To build the U-Boot:
* make sure you have cross-toolchains for arm (arm-linux-gnueabi-) and arm 64bit (aarch64-linux-gnu-); if needed put them in your PATH
  * if you run Debian or Ubuntu, the easiest way is to install the packages gcc-arm-linux-gnueabi and gcc-aarch64-linux-gnu, but you could also use Linaro toolchains (this is what Armbian uses)
* clone out the actual sw repos:
  * [ATF (Marvell fork)](https://github.com/MarvellEmbeddedProcessors/atf-marvell), branch atf-v1.5-armada-18.12
  * [U-Boot (Marvell fork)](https://github.com/MarvellEmbeddedProcessors/u-boot-marvell.git), branch u-boot-2018.03-armada-18.12
  * [MV-DDR](https://github.com/MarvellEmbeddedProcessors/mv-ddr-marvell.git), branch mv_ddr-armada-18.12
  * [A3700-utils](https://github.com/MarvellEmbeddedProcessors/A3700-utils-marvell), branch branch A3700_utils-armada-18.12
* edit espressobin-build-u-boot.sh:
  * adapt ATF_SRC, UBOOT_SRC, MV_DDR_SRCA3700_UTILS_SRC to point to your clones of the repos
  * edit CLOCKSPRESET and DDR_TOPOLOGY to match your board (there is some more description on how to choose there in espressobin-build-u-boot.sh)
* apply any patches you might want
  * I'd recommend looking what patches Armbian has for ATF and U-Boot for the Espressobin; currently there seems to be [one patch for U-Boot](https://github.com/armbian/build/tree/master/patch/u-boot/u-boot-mvebu64) and [no patches for ATF](https://github.com/armbian/build/tree/master/patch/atf) for the Espressobin.
   * I'd recommend branching off the respective upstream branch and adding the patches as commit into that branch. This way you can use git clean without losing the changes you want to keep.

With these steps done, you can build U-Boot simply by: `./build-espressobin-uboot.sh`

If all went well, you should see, in the end, something like:
```
ATF-wrapped U-Boot 2018.03-devel-18.12.3"-armbian-custom"+ for the Espressobin in config:
* CLOCKSPRESET=CPU_1000_DDR_800
* DDR_TOPOLOGY=2
is in /mnt/data/espressobin/atf-marvell/build/a3700/debug
```

The actual ATF-wrapped U-boot is produced in the file `flash-image.bin`. This can be installed e.g. by copying to a USB drive (simple ext4 filesystem worked for me, but I don't know if it stock U-Boot recognizes ext4), plugging it into the Espressobin (while official wiki says you should use the USB3 port for this, even the USB2 port works for me), stopping boot (just press enter a few times early on boot; when you get the prompt `Marvell>>`, that's it) and typing in U-Boot prompt:
```
bubt flash-image.bin spi usb
```

There is some more info on updating the bootloader in the [wiki](http://wiki.espressobin.net/tiki-index.php?page=Update+the+Bootloader). It's also possible to flash U-Boot via [SATA](http://wiki.espressobin.net/tiki-index.php?page=Bootloader+recovery+via+SATA) or [serial](http://wiki.espressobin.net/tiki-index.php?page=Bootloader+recovery+via+UART). This should work  when the board does not boot into U-Boot, e.g. if you managed to flash a broken version.

## Building Linux kernel
This assumes you're building the mainline kernel with some extra patches by Armbian.

To build the kernel:
* make sure you have cross-toolchain for arm 64bit (aarch64-linux-gnu-); if needed put it in your PATH
  * if you run Debian or Ubuntu, the easiest way is to install the package gcc-aarch64-linux-gnu, but you could also use Linaro toolchain (this is what Armbian uses)
* clone the linux-stable repo, branch linux-4.19.y
  * you can get it from [git.kernel.org](https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git/) or from some mirrors (Armbian uses [kernel.googlesource.com](https://kernel.googlesource.com/pub/scm/linux/kernel/git/stable/linux-stable))
* edit build-espressobin-kernel.sh:
  * adapt KERNEL_BUILD_DIR to point to your clones of the kernel repo
* apply patches you might want
  * Armbian has a series of kernel patches for the Espressobin in [patch/kernel/mvebu64-next](https://github.com/armbian/build/tree/master/patch/kernel/mvebu64-next)
  * unfortunately not all the patches can be applied by a simple git am as some are plain diffs
  * I recommend branching off from linux-4.19.y, then applying the patches via the script apply-patches.sh (in this repo) - it will use `git am` for all patches that can be applied by it and fall-back to `patch` + `git commit` for the rest
* create a kernel configuration
  * the easiest is to start from [Armbian's config file](https://github.com/armbian/build/blob/master/config/kernel/linux-mvebu64-next.config) - just copy it to .config in the kernel repo

With these steps done, you can build the kernel simply by: `./build-espressobin-kernel.sh`

It will take some time, but if all went well, there should be a new directory `kernel-build/kernel-4.19...` with the resulting kernel image, device tree binaries and modules.

To install these on the Espressobin with Armbian:

1. copy modules from `modules/4.19...` to Espressobin's `/lib/modules`
1. copy device tree binaries from `dtb-4.19...`, kernel image `vmlinuz-4.19...` and kernel config `config-4.19...` to Espressobin's '/boot'
1. (on the Espressobin) create initramfs for the new kernel by: `update-initramfs -c -k 4.19...`
1. adapt symlinks in `/boot`:
  * dtb → dtb-4.19...
  * Image → vmlinuz-4.19...
  * uInitrd → uInitrd-4.19...

With some luck you have a working kernel. :-)

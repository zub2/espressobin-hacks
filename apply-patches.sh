#!/bin/bash

if [ $# -ne 1 ]; then
	echo "Usage: $0 [patches-directory|patch-file]"
	echo
	echo "Applies all files in patches-directory, or a single patch, to git repo"
	echo "in current directory, by the first of the following that succeed:"
	echo "1) git am"
	echo "2) git apply + git commit"
	echo "3) patch + git commit"
	echo
	echo "The working copy needs to be clean and there must be no untracked files."
	exit 1
fi

err_echo() {
	echo "$@" 1>&2
}

apply_patch_git_am() {
	local p="$1"

	if ! git am "$p"; then
		git am --abort
		return 1
	fi
}

apply_patch_git_apply() {
	local p="$1"

	if git apply --index "$p"; then
		git commit -m "applied patch (git am): `basename $p`"
	else
		return 1
	fi
}

apply_patch_patch() {
	local p="$1"

	# not sure that it helps, but do what Armbian build scripts do
	# https://github.com/armbian/build/blob/5659f7fffa3b1a7d2b91f262d339bddb4e60dded/lib/compilation.sh#L582
	# delete files that will be created by the patch
	lsdiff -s --strip=1 "$p" | grep '^+' | awk '{print $2}' | (
		while read f; do
			echo ">> deleting '$f' which will be created by the patch"
			rm -f "$f"
		done
	)

	if patch -p1 -N --batch --silent < "$p"; then
		# add modified and added files
		lsdiff -s --strip=1 "$p" | grep '^[+!]' | awk '{print $2}' | xargs -d '\n' -r git add --

		# mark deleted files as deleted
		lsdiff -s --strip=1 "$p" | grep '^-' | awk '{print $2}' | xargs -d '\n' -r git rm --

		git commit -m "applied patch (patch): `basename $p`"
	else
		git reset --hard
		find . -name \*.rej -print0 | xargs -0 rm --

		return 1
	fi
}

apply_patch() {
	local p="$1"

	echo "> applying patch $p..."
	echo ">> trying git am..."

	if apply_patch_git_am "$p"; then
		echo ">> git am succeeded"
	else
		err_echo ">> git am failed"
		echo ">> trying git apply..."

		if apply_patch_git_apply "$p"; then
			echo ">> git apply succeeded"
		else
			err_echo ">> git apply failed"
			echo ">> trying patch..."

			if apply_patch_patch "$p"; then
				echo ">> patch succeeded"
			else
				err_echo ">> patch failed"
				err_echo ">> Can't apply patch: $p"

				# TODO: add option to continue
				exit 1
			fi
		fi
	fi
}

# first make sure working copy is clean as we do things like git reset --hard
# (when recovering from a failed patch...)
if [ -n "$(git status --porcelain)" ]; then
	err_echo "There are changes (or untracked files) in the working copy. Please make sure"
	err_echo "the working copy is clean before you run this script."
	exit 1
fi

if [ -f "$1" ]; then
	apply_patch "$1"
elif [ -d "$1" ]; then
	for p in "$1"/*.patch; do
		apply_patch "$p"
	done
else
	err_echo "'$1' does not exist!"
	exit 1
fi

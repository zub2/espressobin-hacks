#!/bin/bash
set -e

export ARCH=arm64
export CROSS_COMPILE=aarch64-linux-gnu-

# path to kernel sources with the propper .config
KERNEL_BUILD_DIR=/mnt/data/linux

if [ ! -d "$KERNEL_BUILD_DIR" ]; then
	echo "Error: A3700-utils repo missing or script not configured." 2>&1
	exit 1
fi

# prefix where to install
KERNEL_INSTALL_DIR=kernel-build

# KERNEL_INSTALL_DIR must be an absolute path
KERNEL_INSTALL_DIR=`realpath "$KERNEL_INSTALL_DIR"`

MAKE_ARGS="-j `nproc`"

KERNEL_IMAGE=Image
KERNEL_IMAGE_INSTALL_NAME=vmlinuz
MAKE_TARGETS=(Image dtbs modules)
DTB_DIR=marvell

(
	cd "$KERNEL_BUILD_DIR"

	if [ ! -f ".config" ]; then
		echo "Kernel needs to be configured first."
		echo "You can use the config from Armbian build scripts:"
		echo "https://github.com/armbian/build/blob/master/config/kernel/linux-mvebu64-next.config"
		exit 1
	fi

	make prepare

	# retrieve kernel release
	KERNEL_RELEASE=`make kernelrelease`

	KERNEL_INSTALL_PREFIX="$KERNEL_INSTALL_DIR/kernel-"

	# check if a kernel with the same release name is already installed
	INSTALL_DIR="${KERNEL_INSTALL_PREFIX}$KERNEL_RELEASE"
	COUNTER=0
	while [ -d "$INSTALL_DIR" ]; do
		COUNTER=$((COUNTER+1))
		INSTALL_DIR="${KERNEL_INSTALL_PREFIX}${KERNEL_RELEASE}_`printf %03d $COUNTER`"
	done
	echo "Installing to $INSTALL_DIR"

	MOD_INSTALL_DIR="$INSTALL_DIR/modules"

	mkdir -p "$INSTALL_DIR"

	# build
	make $MAKE_ARGS "${MAKE_TARGETS[@]}"

	# install modules
	make INSTALL_MOD_PATH="$MOD_INSTALL_DIR" modules_install

	# delete useless stuff
	rm -f "$MOD_INSTALL_DIR/lib/modules/$KERNEL_RELEASE/build"
	rm -f "$MOD_INSTALL_DIR/lib/modules/$KERNEL_RELEASE/source"

	# prune nunecessary levels
	mv "$MOD_INSTALL_DIR/lib/modules/$KERNEL_RELEASE" "$MOD_INSTALL_DIR"
	rmdir "$MOD_INSTALL_DIR/lib/modules/"
	rmdir "$MOD_INSTALL_DIR/lib/"

	# install kernel binary itself
	cp -v "arch/$ARCH/boot/$KERNEL_IMAGE" "$INSTALL_DIR/$KERNEL_IMAGE_INSTALL_NAME-$KERNEL_RELEASE"

	# install DTBs
	mkdir -p "$INSTALL_DIR/dtb-$KERNEL_RELEASE/$DTB_DIR"
	cp -v "arch/$ARCH/boot/dts/$DTB_DIR"/*.dtb "$INSTALL_DIR/dtb-$KERNEL_RELEASE/$DTB_DIR"

	# install kernel configuration
	cp -v .config "$INSTALL_DIR/config-$KERNEL_RELEASE"

	echo "Installed to: $INSTALL_DIR"
)

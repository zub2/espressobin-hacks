#!/bin/bash
set -e
#
# Script that builds U-boot for the Espressobin board.
#
# It is based on Armbian build scripts, revision
# 5659f7fffa3b1a7d2b91f262d339bddb4e60dded (Dec 30, 2018).
#
# To use it, there are some things you need to do first:
#
# * check out required sources - see below (ATF, U-Boot, A3700 utils, mv-ddr-marvell)
#
# * set paths (ESPRESSOBIN_SRC_ROOT, ATF_SRC, ...)
#
# * select the board model to build for (set CLOCKSPRESET and DDR_TOPOLOGY)
#
# * make sure you have the necessary dependencies - apart from the usual build
# tools like make, you need arm (32bit arm) and aarch64 (64bit arm) cross-compilers
#
# Apply necessary patches. You can take these from Armbian build scripts.
# As of now there seems to be just 1 patch for the u-boot 2018.03-armada-18.12:
# https://github.com/armbian/build/tree/master/patch/u-boot/u-boot-mvebu64
#
# This scrips is not trying to automate everything, after all Armbian already does
# that. But it's truing to make the U-Boot build for the Espressobin more
# understandable.
#

ESPRESSOBIN_SRC_ROOT=/mnt/data/espressobin

# https://github.com/MarvellEmbeddedProcessors/atf-marvell branch atf-v1.5-armada-18.12
ATF_SRC="$ESPRESSOBIN_SRC_ROOT/atf-marvell"

# https://github.com/MarvellEmbeddedProcessors/u-boot-marvell.git branch u-boot-2018.03-armada-18.12
UBOOT_SRC="$ESPRESSOBIN_SRC_ROOT/u-boot-marvell"

# https://github.com/MarvellEmbeddedProcessors/mv-ddr-marvell.git branch mv_ddr-armada-18.12
MV_DDR_SRC="$ESPRESSOBIN_SRC_ROOT/mv-ddr-marvell"

# A3700-utils: https://github.com/MarvellEmbeddedProcessors/A3700-utils-marvell branch A3700_utils-armada-18.12
A3700_UTILS_SRC="$ESPRESSOBIN_SRC_ROOT/A3700-utils-marvell"

# Pick one of:
# * CPU_600_DDR_600
# * CPU_800_DDR_800
# * CPU_1000_DDR_800
# * CPU_1200_DDR_750
#
# You can see the CPU and DDR frequency in serial output when you boot the board,
# e.g.:
#
# Model: Marvell Armada 3720 Community Board ESPRESSOBin
#       CPU     1000 [MHz]
#       L2      800 [MHz]
#       TClock  200 [MHz]
#       DDR     800 [MHz]
#
CLOCKSPRESET=CPU_1000_DDR_800

# Choose this depending on the memory configuration of your board:
#
# DDR3, 512MiB, 1 chip:  0
# DDR3, 512MiB, 2 chips: 0
#
# DDR3, 1GiB, 2 chips: 2
# DDR3, 1GiB, 1 chip:  4
#
# DDR3, 2GiB, 2 chipds: 2
#
# DDR4, 1GiB, 1 chip: 5
# DDR4, 1GiB, 2 chipds: 6
#
# AFAIK only board revisions ≥ 7 use DDR4 memory.
#
# The list is based on Armbian's build script:
# https://github.com/armbian/build/blob/5659f7fffa3b1a7d2b91f262d339bddb4e60dded/config/sources/mvebu64.conf#L83
#
DDR_TOPOLOGY=2

CROSS_PREFIX_AARCH64=aarch64-linux-gnu-
CROSS_PREFIX_ARM=arm-linux-gnueabi-

# How many jobs to run in parallel
MAKE_JOBS=`nproc`

if [ "$1" == "clean" ]; then
	echo "This will run git clean -fdx in:"
	echo "* $ATF_SRC"
	echo "* $UBOOT_SRC"
	echo "* $MV_DDR_SRC"
	echo "* $A3700_UTILS_SRC"
	echo
	echo "(It seems make clean is not sufficient... Thanks, Marvell!)"
	echo
	read -p "All your uncommitted changes in these repos will be deleted. Are you sure? " p
	if [ "$p" == "yes" ]; then
		( cd "$ARF_SRC" && git clean -fdx )
		( cd "$UBOOT_SRC" && git clean -fdx )
		( cd "$MV_DDR_SRC" && git clean -fdx )
		( cd "$A3700_UTILS_SRC" && git clean -fdx )
	fi

	exit 0
fi

if [ ! -d "$ATF_SRC" ]; then
	echo "Error: ATF repo missing or script not configured." 2>&1
	exit 1
fi
if [ ! -d "$UBOOT_SRC" ]; then
	echo "Error: U-Boot repo missing or script not configured." 2>&1
	exit 1
fi
if [ ! -d "$MV_DDR_SRC" ]; then
	echo "Error: mv-ddr repo missing or script not configured." 2>&1
	exit 1
fi
if [ ! -d "$A3700_UTILS_SRC" ]; then
	echo "Error: A3700-utils repo missing or script not configured." 2>&1
	exit 1
fi

# Check if U-Boot has been configured
if [ ! -f "$UBOOT_SRC/.config" ]; then
	echo "U-Boot needs to be configured before you build it."
	echo "To use default config, you can do:"
	echo "	cd $UBOOT_SRC && make mvebu_espressobin-88f3720_defconfig"
	echo "or use the modified uboot-mvebu_espressobin-armbian_config:"
	echo "	cp uboot-mvebu_espressobin-armbian_config $UBOOT_SRC/.config"

	exit 1
fi

# First build ATF's bl31.bin
(
	echo "Building ATF's bl32.bin..."
	cd "$ATF_SRC"

	# it seems that using fixed values for CLOCKSPRESET and DDR_TOPOLOGY it OK for this step
	# https://github.com/armbian/build/blob/5659f7fffa3b1a7d2b91f262d339bddb4e60dded/config/sources/mvebu64.conf#L20
	make \
		-j "$MAKE_JOBS" \
		DEBUG=1 \
		USE_COHERENT_MEM=0 \
		LOG_LEVEL=20 \
		SECURE=0 \
		CLOCKSPRESET=CPU_800_DDR_800 \
		DDR_TOPOLOGY=2 \
		BOOTDEV=SPINOR \
		PARTNUM=0 \
		PLAT=a3700 \
		CROSS_COMPILE="$CROSS_PREFIX_AARCH64"
)

# Now build U-Boot
(
	echo "Building U-Boot..."
	cd "$UBOOT_SRC"

	# olddefconfig is needed if the .config is the modified defconfig
	make olddefconfig

	# copy bl32.bin built in the previous step into current directory
	cp -v "$ATF_SRC/build/a3700/debug/bl31.bin" .

	make \
		-j "$MAKE_JOBS" \
		DEVICE_TREE=armada-3720-espressobin \
		CROSS_COMPILE="$CROSS_PREFIX_AARCH64" \
		all
)

# Wrap U-Boot in ATF
(
	echo "Wrapping U-Boot in ATF..."
	cd "$ATF_SRC"

	make \
		-j "$MAKE_JOBS" \
		MV_DDR_PATH="$MV_DDR_SRC" \
		CROSS_COMPILE="$CROSS_PREFIX_AARCH64" \
		DEBUG=1 \
		USE_COHERENT_MEM=0 \
		LOG_LEVEL=20 \
		SECURE=0 \
		CLOCKSPRESET=$CLOCKSPRESET \
		DDR_TOPOLOGY=$DDR_TOPOLOGY \
		BOOTDEV=SPINOR \
		PARTNUM=0 \
		PLAT=a3700 \
		BL33="$UBOOT_SRC/u-boot.bin" \
		WTP="$A3700_UTILS_SRC" \
		CROSS_CM3="$CROSS_PREFIX_ARM" \
		all fip
)

pushd "$UBOOT_SRC" > /dev/null
UBOOT_RELEASE=`make CROSS_COMPILE="$CROSS_PREFIX_AARCH64" ubootrelease`
popd > /dev/null

echo
echo "ATF-wrapped U-Boot $UBOOT_RELEASE for the Espressobin in config:"
echo "* CLOCKSPRESET=$CLOCKSPRESET"
echo "* DDR_TOPOLOGY=$DDR_TOPOLOGY"
echo "is in $ATF_SRC/build/a3700/debug"
